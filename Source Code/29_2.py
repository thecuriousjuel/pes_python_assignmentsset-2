string = 'lorem IPSUM \t olor SIT amet'

print('Insert spaces instead of tab character : ', string.expandtabs(20))
print('Encode String : ', string.encode())
print('Find substring and return index : ', string.find('olor'))
print('Format this : %s' % string)
print('Index of SIT : ', string.index('SIT'))
